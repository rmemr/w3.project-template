@echo off
rem ---------------------------------------------------
rem --- settings
rem ---------------------------------------------------
call ../_settings_.bat

rem ---------------------------------------------------
rem --- check for settings
rem ---------------------------------------------------
IF %SETTINGS_LOADED% EQU 1 goto :SettingsLoaded

echo ERROR! Settings not loaded! - do not start this file directly!
EXIT /B 1
rem ---------------------------------------------------
:SettingsLoaded

:ConvertAudio
echo.
echo --------------------------------------------------------------------------
echo -- CONVERTING AUDIO FILES %PATCHING%
echo --------------------------------------------------------------------------
echo.
echo  ^>^> COLLECTING AUDIO LIST
echo.

CALL "%DIR_SPEECH%\wwise\_generate.sources.bat"

IF %ERRORLEVEL% NEQ 0 GOTO SomeError

echo.
echo  ^>^> CONVERTING AUDIO FILES TO *.wem
echo.

if "%LOG_LEVEL%" NEQ "" SET WWISE_LOGLEVEL=--verbose

set DIR_AUDIO_WEM_CLASSIC=%DIR_AUDIO_WEM%.classic

if EXIST "%DIR_AUDIO_WEM_CLASSIC%\Wwise.dat" (
  del /F /AH "%DIR_AUDIO_WEM_CLASSIC%\Wwise.dat"
)

"%DIR_WWISE_BIN_CLASSIC%\WwiseCLI.exe" "%DIR_SPEECH%\wwise\classic\Classic-Conversion.wproj" -ConvertExternalSources -ExternalSourcesOutput WINDOWS "%DIR_AUDIO_WEM_CLASSIC%" %WWISE_LOGLEVEL%

IF %ERRORLEVEL% NEQ 0 GOTO SomeError

del /F /AH "%DIR_AUDIO_WEM_CLASSIC%\Wwise.dat"

exit /B 0

rem ---------------------------------------------------
:SomeError
echo.
echo ERROR! Something went WRONG! Audio files were NOT CONVERTED!
echo.
exit /B 1

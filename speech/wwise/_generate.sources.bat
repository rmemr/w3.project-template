@echo off

rem set DIR_PROJECT_BIN=%DIR_PROJECT_BASE%\bin
rem set DIR_SPEECH=%DIR_PROJECT_BASE%\speech

if "%language%" == "" set language=en
set DIR_AUDIO_WAV=%DIR_SPEECH%\speech.%language%.wav
SET FILE_SOURCES="%DIR_SPEECH%\wwise\ext-sources.wsources"

echo ^<?xml version="1.0" encoding="UTF-8"?^>  > %FILE_SOURCES%
echo ^<ExternalSourcesList SchemaVersion="1" Root="%DIR_AUDIO_WAV%"^> >> %FILE_SOURCES%

for %%f in ("%DIR_AUDIO_WAV%\*") do (
    if "%%~xf"==".wav" (
        echo    ^<Source Path="%%~nxf" Conversion="Default Conversion Settings" /^> >> %FILE_SOURCES%
    )
    if "%%~xf"==".ogg" (
        echo    ^<Source Path="%%~nxf" Conversion="Default Conversion Settings" /^> >> %FILE_SOURCES%
    )
)

echo ^</ExternalSourcesList^> >> %FILE_SOURCES%
